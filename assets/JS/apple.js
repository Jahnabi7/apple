// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.collapse')) {
      var dropdowns = document.getElementsByClassName("list-group list-group-flush");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }
  